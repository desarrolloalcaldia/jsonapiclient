﻿/// Author:         Anthony Baker
/// Date:           May 4th, 2013
/// Description:    JsonAPIClient - Sample JSON API Consumption

using System;
using System.IO;
using System.Net;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Collections.Specialized;
using System.Net.NetworkInformation;


namespace JsonApiClient
{
    public static class webservice
    {
        #region fields

 

         private const string baseUrl = "http://192.168.104.117/cobrosnotributarios/web/index.php?r=tramites/ws-mt-comprobante-valores/create";

        #endregion

        #region methods

      
        public static void GetWebservice(string tramite_id,string comp_val_precio, string gestion_id,string nombres, string ap_paterno, string ap_materno, string num_sec_tipo_documento, string ci, string lugar_ci, string direccion,string usuario) 
        {
            // Customize URL according to geo location parameters
            var url = string.Format(baseUrl);


            // Syncronious Consumption
            var syncClient = new WebClient();

        
            NameValueCollection postData = new NameValueCollection()
                {
              { "tram_id", tramite_id},  //order: {"parameter name", "parameter value"}
              { "comp_val_precio", comp_val_precio },
              { "gest_id", gestion_id },
              { "nombres", nombres},
              { "ap_paterno", ap_paterno},
              { "ap_materno", ap_materno},
              { "num_sec_tipo_documento", num_sec_tipo_documento},
              { "ci", ci},
              { "lugar_ci", lugar_ci},
              { "direccion", nombres},
              { "usuario", usuario},
              
             };

            // client.UploadValues returns page's source as byte array (byte[])
            // so it must be transformed into a string
            string pagesource = Encoding.UTF8.GetString(syncClient.UploadValues(url, postData));

            Console.WriteLine(pagesource);


           
        }
        //funcion para verificar si hay respuesta del servidor
        public static bool PingHost(string nameOrAddress)
        {
            bool pingable = false;
            Ping pinger = new Ping();
            try
            {
                PingReply reply = pinger.Send(nameOrAddress);
                pingable = reply.Status == IPStatus.Success;
            }
            catch (PingException)
            {
                // Descartar los PingExceptions y returna false;
            }
            return pingable;
        }


        #endregion
    }
}
