﻿/// Author:         Anthony Baker
/// Date:           May 4th, 2013
/// Description:    JsonAPIClient - Sample JSON API Consumption

using System;

namespace JsonApiClient
{
    class Program
    {
        static void Main(string[] args)
        {

            string tramite_id = "20";
            string comp_val_precio = "30";
            string gestion_id = "1";
            string nombres = "jose";
            string ap_paterno = "perez";
            string ap_materno = "ramos";
            string num_sec_tipo_documento = "1";
            string ci = "6456661";
            string lugar_ci = "CBA";
            string direccion = "Av America";
            string usuario = "usuario_catasro";

            if (webservice.PingHost("192.168.20.117")) { 
                webservice.GetWebservice( tramite_id,  comp_val_precio,  gestion_id,  nombres,  ap_paterno,  ap_materno,  num_sec_tipo_documento,  ci,  lugar_ci,  direccion,  usuario);
            }
            else
            {
                //esperamos como 2 a 4 segundos aprox para que nos responda si falla
                Console.WriteLine("no me puedo conectar");
            }

            Console.ReadLine();
        }

       
    }
}
